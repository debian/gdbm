# French translation of gdbm.
# Copyright (C) 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the gdbm package.
# Frédéric Marchal <fmarchal@perso.be>, 2024.
#
# Je ne désire pas continuer cette traduction. Quiconque voudrait prendre la relève
# est le bienvenu.
#
msgid ""
msgstr ""
"Project-Id-Version: gdbm 1.23.90\n"
"Report-Msgid-Bugs-To: bug-gdbm@gnu.org\n"
"POT-Creation-Date: 2024-07-01 09:33+0300\n"
"PO-Revision-Date: 2024-06-18 15:54+0200\n"
"Last-Translator: Frédéric Marchal <fmarchal@perso.be>\n"
"Language-Team: French <traduc@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=2; plural=(n >= 2);\n"

#: src/bucket.c:399 src/bucket.c:685 src/falloc.c:211 src/falloc.c:337
#: src/findkey.c:124 src/gdbmstore.c:168 src/update.c:36 src/update.c:76
msgid "lseek error"
msgstr "Erreur lseek"

#: src/bucket.c:534
msgid "directory overflow"
msgstr "débordement de répertoire"

#: src/bucket.c:545 src/falloc.c:302 src/findkey.c:97 src/findkey.c:113
msgid "malloc error"
msgstr "Erreur malloc"

#: src/falloc.c:201
msgid "malloc failed"
msgstr "malloc a échoué"

#: src/gdbmerrno.c:102
msgid "No error"
msgstr "Pas d'erreur"

#: src/gdbmerrno.c:103
msgid "Malloc error"
msgstr "Erreur malloc"

#: src/gdbmerrno.c:104
msgid "Block size error"
msgstr "Erreur de taille de bloc"

#: src/gdbmerrno.c:105
msgid "File open error"
msgstr "Erreur à l'ouverture du fichier"

#: src/gdbmerrno.c:106
msgid "File write error"
msgstr "Erreur d'écriture dans le fichier"

#: src/gdbmerrno.c:107
msgid "File seek error"
msgstr "Erreur de positionnement dans le fichier"

#: src/gdbmerrno.c:108
msgid "File read error"
msgstr "Erreur de lecture dans le fichier"

#: src/gdbmerrno.c:109
msgid "Bad magic number"
msgstr "Mauvais nombre magique"

#: src/gdbmerrno.c:110
msgid "Empty database"
msgstr "Base de données vide"

#: src/gdbmerrno.c:111
msgid "Can't be reader"
msgstr "Ne peut être un lecteur"

#: src/gdbmerrno.c:112
msgid "Can't be writer"
msgstr "Ne peut être celui qui écrit"

#: src/gdbmerrno.c:113
msgid "Reader can't delete"
msgstr "Un lecteur ne peut pas supprimer"

#: src/gdbmerrno.c:114
msgid "Reader can't store"
msgstr "Un lecteur ne peut pas stocker"

#: src/gdbmerrno.c:115
msgid "Reader can't reorganize"
msgstr "Un lecteur ne peut pas réorganiser"

#: src/gdbmerrno.c:116
msgid "Should not happen: unused error code"
msgstr "Ne devrait pas se produire: code d'erreur inutilisé"

#: src/gdbmerrno.c:117
msgid "Item not found"
msgstr "Élément pas trouvé"

#: src/gdbmerrno.c:118
msgid "Reorganize failed"
msgstr "La réorganisation a échoué"

#: src/gdbmerrno.c:119
msgid "Cannot replace"
msgstr "Remplacement impossible"

#: src/gdbmerrno.c:120
msgid "Malformed data"
msgstr "Donnée mal formée"

#: src/gdbmerrno.c:121
msgid "Option already set"
msgstr "Option déjà fixée"

#: src/gdbmerrno.c:122
msgid "Bad option value"
msgstr "Mauvaise valeur pour l'option"

#: src/gdbmerrno.c:123
msgid "Byte-swapped file"
msgstr "Fichier à octets permutés"

#: src/gdbmerrno.c:124
msgid "File header assumes wrong off_t size"
msgstr "L'en-tête du fichier suppose la mauvaise taille off_t"

#: src/gdbmerrno.c:125
msgid "Bad file flags"
msgstr "Mauvais fanions de fichier"

#: src/gdbmerrno.c:126
msgid "Cannot stat file"
msgstr "stat échoue sur le fichier"

#: src/gdbmerrno.c:127
msgid "Unexpected end of file"
msgstr "Fin du fichier inattendue"

#: src/gdbmerrno.c:128
msgid "Database name not given"
msgstr "Nom de base de données non fourni"

#: src/gdbmerrno.c:129
msgid "Failed to restore file owner"
msgstr "Échec lors du rétablissement du propriétaire"

#: src/gdbmerrno.c:130
msgid "Failed to restore file mode"
msgstr "Échec lors du rétablissement du mode du fichier"

#: src/gdbmerrno.c:131
msgid "Database needs recovery"
msgstr "La base de données a besoin d'être récupérée"

#: src/gdbmerrno.c:132
msgid "Failed to create backup copy"
msgstr "Échec lors de la création d'une copie de sauvegarde"

#: src/gdbmerrno.c:133
msgid "Bucket directory overflow"
msgstr "Débordement du répertoire de compartimentage"

#: src/gdbmerrno.c:134
msgid "Malformed bucket header"
msgstr "En-tête de compartiment mal formé"

#: src/gdbmerrno.c:135
msgid "Malformed database file header"
msgstr "L'en-tête du fichier de base de données est mal formé"

#. TRANSLATORS: avail_block is a field name. Don't translate it.
#: src/gdbmerrno.c:137
msgid "Malformed avail_block"
msgstr "avail_block mal formé"

#: src/gdbmerrno.c:138
msgid "Malformed hash table"
msgstr "Table de hachage mal formée"

#: src/gdbmerrno.c:139
msgid "Invalid directory entry"
msgstr "Entrée de répertoire invalide"

#: src/gdbmerrno.c:140
msgid "Error closing file"
msgstr "Erreur à la fermeture du fichier"

#: src/gdbmerrno.c:141
msgid "Error synchronizing file"
msgstr "Erreur durant la synchronisation du fichier"

#: src/gdbmerrno.c:142
msgid "Error truncating file"
msgstr "Erreur en tronquant le fichier"

#: src/gdbmerrno.c:143
msgid "Bucket cache corrupted"
msgstr "La cache compartimentée est corrompue"

#: src/gdbmerrno.c:144
msgid "Malformed bucket hash entry"
msgstr "Entrée de hachage de compartiment mal formée"

#: src/gdbmerrno.c:145
msgid "Reflink failed"
msgstr "La liaison par référence (reflink) a échoué"

#: src/gdbmerrno.c:146
msgid "Failed to resolve real path name"
msgstr "Échec lors de la résolution du nom de chemin réel"

#: src/gdbmerrno.c:147
msgid "Function usage error"
msgstr "Erreur d'utilisation de la fonction"

#: src/recover.c:272
#, c-format
msgid "can't read bucket #%d: %s"
msgstr "impossible de lire le compartiment #%d : %s"

#: src/recover.c:300
#, c-format
msgid "can't read key pair %d:%d (%lu:%d): %s"
msgstr "impossible de lire la paire de clés %d:%d (%lu:%d) : %s"

#: src/recover.c:330
#, c-format
msgid "ignoring duplicate key %d:%d (%lu:%d)"
msgstr "la clé dupliquée %d:%d (%lu:%d) est ignorée"

#: src/recover.c:340
#, c-format
msgid "fatal: can't store element %d:%d (%lu:%d): %s"
msgstr "fatal: impossible de stocker l'élément %d:%d (%lu:%d) : %s"

#: tools/datconv.c:291
#, c-format
msgid "(not enough data)"
msgstr "(pas assez de données)"

#: tools/datconv.c:383
msgid "cannot convert"
msgstr "impossible de convertir"

#: tools/datconv.c:392
#, c-format
msgid "cannot convert value #%d: %s"
msgstr "impossible de convertir la valeur #%d: %s"

#: tools/datconv.c:418 tools/datconv.c:467
msgid "mixing tagged and untagged values is not allowed"
msgstr "mélanger des valeurs étiquetées et non étiquetées n'est pas permis"

#: tools/datconv.c:472
#, c-format
msgid "%s: no such field in datum"
msgstr "%s: fichier non trouvé dans les données"

#: tools/gdbm_dump.c:22
msgid "dump a GDBM database to a file"
msgstr "vidanger une base de données GDBM dans un fichier"

#: tools/gdbm_dump.c:23
msgid "DB_FILE [FILE]"
msgstr "FICHIER_DB [FICHIER]"

#: tools/gdbm_dump.c:25
msgid "select dump format"
msgstr "sélectionnez le format de décharge"

#: tools/gdbm_dump.c:67 tools/gdbm_dump.c:79
msgid "unknown dump format"
msgstr "format de décharge inconnu"

#: tools/gdbm_dump.c:86 tools/gdbm_load.c:225
msgid "unknown option"
msgstr "option inconnue"

#: tools/gdbm_dump.c:102 tools/gdbm_load.c:241
#, c-format
msgid "too many arguments; try `%s -h' for more info"
msgstr "trop d'arguments; essayez « %s -h » pour obtenir plus d'informations"

#: tools/gdbm_dump.c:122 tools/gdbm_load.c:267
#, c-format
msgid "cannot open %s"
msgstr "impossible d'ouvrir %s"

#: tools/gdbm_dump.c:130 tools/gdbm_load.c:277
msgid "gdbm_open failed"
msgstr "échec de gdbm_open"

#: tools/gdbm_dump.c:137
#, c-format
msgid "%s: dump error"
msgstr "%s: erreur de décharge"

#: tools/gdbm_load.c:32
msgid "load a GDBM database from a file"
msgstr "charger une base de données GDBM depuis un fichier"

#: tools/gdbm_load.c:33
msgid "FILE [DB_FILE]"
msgstr "FICHIER [FICHIER_DB]"

#: tools/gdbm_load.c:35
msgid "replace records in the existing database (needs -U)"
msgstr ""
"remplacer les enregistrements dans la base de données existante (nécessite -"
"U)"

#: tools/gdbm_load.c:36
msgid "MODE"
msgstr "MODE"

#: tools/gdbm_load.c:36
msgid "set file mode"
msgstr "changer le mode du fichier"

#: tools/gdbm_load.c:37
msgid "update the existing database"
msgstr "mettre à jour la base de données existante"

#: tools/gdbm_load.c:38
msgid "NAME|UID[:NAME|GID]"
msgstr "NOM|UID[:NOM|GID]"

#: tools/gdbm_load.c:38
msgid "set file owner"
msgstr "changer le propriétaire du fichier"

#: tools/gdbm_load.c:39
msgid "do not attempt to set file meta-data"
msgstr "n'essaie pas de changer les méta données du fichier"

#: tools/gdbm_load.c:40
msgid "use memory mapping"
msgstr "utiliser la concordance mémoire"

#: tools/gdbm_load.c:41 tools/gdbm_load.c:42
msgid "NUM"
msgstr "NBR"

#: tools/gdbm_load.c:41
msgid "set the cache size"
msgstr "changer la taille de la cache"

#: tools/gdbm_load.c:42
msgid "set the block size"
msgstr "changer la taille de bloc"

#: tools/gdbm_load.c:80
#, c-format
msgid "invalid number: %s"
msgstr "nombre invalide: %s"

#: tools/gdbm_load.c:85
#, c-format
msgid "invalid number: %s: %s"
msgstr "nombre invalide: %s: %s"

#: tools/gdbm_load.c:137
#, c-format
msgid "invalid octal number"
msgstr "nombre octal invalide"

#: tools/gdbm_load.c:167
#, c-format
msgid "invalid user name: %s"
msgstr "nom d'utilisateur invalide: %s"

#: tools/gdbm_load.c:186
#, c-format
msgid "invalid group name: %s"
msgstr "nom de groupe invalide: %s"

#: tools/gdbm_load.c:198
#, c-format
msgid "no such UID: %lu"
msgstr "cet UID n'existe pas: %lu"

#: tools/gdbm_load.c:247
msgid "-r is useless without -U"
msgstr "-r est inutile sans -U"

#: tools/gdbm_load.c:283
#, c-format
msgid "gdbm_setopt failed: %s"
msgstr "échec de gdbm_setopt: %s"

#: tools/gdbm_load.c:297 tools/gdbm_load.c:315
#, c-format
msgid "error restoring metadata: %s (%s)"
msgstr "erreur lors de la récupération des méta données: %s (%s)"

#: tools/gdbm_load.c:306 tools/gdbmshell.c:1674
#, c-format
msgid "cannot load from %s"
msgstr "impossible de charger depuis %s"

#: tools/gdbm_load.c:323
msgid "gdbm_setopt failed"
msgstr "échec de gdbm_setopt"

#: tools/gdbmshell.c:64 tools/gdbmshell.c:157 tools/gdbmshell.c:361
#: tools/gdbmshell.c:497 tools/gdbmshell.c:905 tools/gdbmshell.c:942
#: tools/gdbmshell.c:951
#, c-format
msgid "%s failed"
msgstr "%s a échoué"

#: tools/gdbmshell.c:120
#, c-format
msgid "database %s already exists; overwrite"
msgstr "la base de données %s existe déjà. Écraser"

#: tools/gdbmshell.c:151
#, c-format
msgid "cannot open database %s"
msgstr "impossible d'ouvrir la base de données %s"

#: tools/gdbmshell.c:252
#, c-format
msgid "Bucket #%d"
msgstr "Compartiment #%d"

#: tools/gdbmshell.c:255
#, c-format
msgid ""
"address     = %lu\n"
"depth       = %d\n"
"hash prefix = %08x\n"
"references  = %u"
msgstr ""
"adresse         = %lu\n"
"profondeur      = %d\n"
"préfixe hachage = %08x\n"
"référence       = %u"

#: tools/gdbmshell.c:270
#, c-format
msgid ""
"count       = %d\n"
"load factor = %3d\n"
msgstr ""
"nombre         = %d\n"
"facteur charge = %3d\n"

#: tools/gdbmshell.c:275
msgid "Hash Table:\n"
msgstr "Table de hachage :\n"

#: tools/gdbmshell.c:277
#, c-format
msgid ""
"    #    hash value     key size    data size     data adr home  key start\n"
msgstr ""
"    #    valeur hash    taille clé  taille donnée adr donnée accueil début "
"clé\n"

#: tools/gdbmshell.c:295
#, c-format
msgid ""
"\n"
"Avail count = %d\n"
msgstr ""
"\n"
"Nombre disponible = %d\n"

#: tools/gdbmshell.c:296
#, c-format
msgid "Address           size\n"
msgstr "Adresse           taille\n"

#: tools/gdbmshell.c:347
msgid "header block"
msgstr "bloc d'en-tête"

#: tools/gdbmshell.c:349
#, c-format
msgid "block = %lu"
msgstr "bloc = %lu"

#: tools/gdbmshell.c:350
#, c-format
msgid ""
"\n"
"size  = %d\n"
"count = %d\n"
msgstr ""
"\n"
"taille = %d\n"
"nombre = %d\n"

#: tools/gdbmshell.c:374
#, c-format
msgid ""
"Bucket Cache (size %zu/%zu):\n"
"  Index:         Address  Changed  Data_Hash \n"
msgstr ""
"Cache compartimentée (taille %zu/%zu):\n"
"  Index:         Adresse  Changé   Data_Hash \n"

#: tools/gdbmshell.c:381
msgid "True"
msgstr "Vrai"

#: tools/gdbmshell.c:381
msgid "False"
msgstr "Faux"

#: tools/gdbmshell.c:386
#, c-format
msgid "Bucket cache is empty.\n"
msgstr "La cache compartimentée est vide.\n"

#: tools/gdbmshell.c:463
msgid "nothing to close"
msgstr "rien à fermer"

#: tools/gdbmshell.c:506
msgid "count buffer overflow"
msgstr "débordement du tampon de comptage"

#: tools/gdbmshell.c:509
#, c-format
msgid "There is %s item in the database.\n"
msgid_plural "There are %s items in the database.\n"
msgstr[0] "Il y a %s élément dans la base de données.\n"
msgstr[1] "Il y a %s éléments dans la base de données.\n"

#: tools/gdbmshell.c:526 tools/gdbmshell.c:550 tools/gdbmshell.c:630
msgid "No such item found"
msgstr "Cet élément n'a pas été trouvé"

#: tools/gdbmshell.c:529
msgid "Can't delete"
msgstr "Ne peut supprimer"

#: tools/gdbmshell.c:553
msgid "Can't fetch data"
msgstr "Ne peut récupérer les données"

#: tools/gdbmshell.c:566
msgid "Item not inserted"
msgstr "Élément pas inséré"

#: tools/gdbmshell.c:594
#, c-format
msgid "No such item found.\n"
msgstr "Cet élément n'a pas été trouvé.\n"

#: tools/gdbmshell.c:597
msgid "Can't find first key"
msgstr "Ne peut trouver la première clé"

#: tools/gdbmshell.c:634
msgid "Can't find next key"
msgstr "Ne peut trouver la clé suivante"

#: tools/gdbmshell.c:645
msgid "Reorganization failed"
msgstr "La réorganisation a échoué"

#: tools/gdbmshell.c:649
msgid "Reorganization succeeded."
msgstr "La réorganisation a réussi."

#: tools/gdbmshell.c:702 tools/gdbmshell.c:751 tools/gdbmshell.c:1599
#: tools/gdbmshell.c:1635
#, c-format
msgid "unrecognized argument: %s"
msgstr "argument non reconnu: %s"

#: tools/gdbmshell.c:710
#, c-format
msgid "%s: bad argument type"
msgstr "%s: mauvais type d'argument"

#: tools/gdbmshell.c:715
msgid "unexpected compound statement"
msgstr "instruction composée inattendue"

#: tools/gdbmshell.c:724 tools/gdbmshell.c:734 tools/gdbmshell.c:744
#: tools/gdbmshell.c:856 tools/gdbmshell.c:865
#, c-format
msgid "not a number (stopped near %s)"
msgstr "pas un nombre (arrêté près de %s)"

#: tools/gdbmshell.c:757
msgid "unexpected datum"
msgstr "données inattendue"

#: tools/gdbmshell.c:767
#, c-format
msgid "Recovery succeeded.\n"
msgstr "La récupération a réussi.\n"

#: tools/gdbmshell.c:771
#, c-format
msgid "Keys recovered: %lu, failed: %lu, duplicate: %lu\n"
msgstr "Clés récupérées: %lu, échouées: %lu, dupliquées: %lu\n"

#: tools/gdbmshell.c:776
#, c-format
msgid "Buckets recovered: %lu, failed: %lu\n"
msgstr "Compartiments récupérés: %lu, échoués: %lu\n"

#: tools/gdbmshell.c:784
#, c-format
msgid "Original database preserved in file %s"
msgstr "La base de données originale a été préservée dans le fichier %s"

#: tools/gdbmshell.c:792
msgid "Recovery failed"
msgstr "Échec de la récupération"

#: tools/gdbmshell.c:843 tools/gdbmshell.c:926
#, c-format
msgid "no current bucket\n"
msgstr "pas de compartiment courant\n"

#: tools/gdbmshell.c:893
#, c-format
msgid "bucket number out of range (0..%lu)"
msgstr "le numéro de compartiment est hors limites (0..%lu)"

#: tools/gdbmshell.c:936 tools/gdbmshell.c:948
#, c-format
msgid "no sibling\n"
msgstr "pas de frère\n"

#: tools/gdbmshell.c:996
#, c-format
msgid "Hash table directory.\n"
msgstr "Répertoire de table de hachage.\n"

#: tools/gdbmshell.c:997
#, c-format
msgid ""
"  Size =  %d.  Capacity = %lu.  Bits = %d,  Buckets = %zu.\n"
"\n"
msgstr ""
"  Taille = %d.  Capacité = %lu.  Bits = %d,  Compartiments = %zu.\n"
"\n"

#: tools/gdbmshell.c:1004
msgid "Index"
msgstr "Index"

#: tools/gdbmshell.c:1004
msgid "Hash Pfx"
msgstr "Pfx Hachage"

#: tools/gdbmshell.c:1004
msgid "Bucket address"
msgstr "Adresse compartiment"

#: tools/gdbmshell.c:1072
#, c-format
msgid ""
"\n"
"File Header: \n"
"\n"
msgstr ""
"\n"
"En-tête fichier: \n"
"\n"

#: tools/gdbmshell.c:1073
#, c-format
msgid "  type            = %s\n"
msgstr "  type                = %s\n"

#: tools/gdbmshell.c:1074
#, c-format
msgid "  directory start = %lu\n"
msgstr "  début du répertoire = %lu\n"

#: tools/gdbmshell.c:1076
#, c-format
msgid "  directory size  = %d\n"
msgstr "  taille répertoire   = %d\n"

#: tools/gdbmshell.c:1077
#, c-format
msgid "  directory depth = %d\n"
msgstr "  profondeur courante = %d\n"

#: tools/gdbmshell.c:1078
#, c-format
msgid "  block size      = %d\n"
msgstr "  taille bloc         = %d\n"

#: tools/gdbmshell.c:1079
#, c-format
msgid "  bucket elems    = %d\n"
msgstr "  elts compartiment   = %d\n"

#: tools/gdbmshell.c:1080
#, c-format
msgid "  bucket size     = %d\n"
msgstr "  taille compartiment = %d\n"

#: tools/gdbmshell.c:1081
#, c-format
msgid "  header magic    = %x\n"
msgstr "  magic en-tête       = %x\n"

#: tools/gdbmshell.c:1082
#, c-format
msgid "  next block      = %lu\n"
msgstr "  bloc suivant        = %lu\n"

#: tools/gdbmshell.c:1085
#, c-format
msgid "  avail size      = %d\n"
msgstr "  taille disponible   = %d\n"

#: tools/gdbmshell.c:1086
#, c-format
msgid "  avail count     = %d\n"
msgstr "  nombre disponible   = %d\n"

#: tools/gdbmshell.c:1087
#, c-format
msgid "  avail next block= %lu\n"
msgstr "  bloc suivant dispo  = %lu\n"

#: tools/gdbmshell.c:1092
#, c-format
msgid ""
"\n"
"Extended Header: \n"
"\n"
msgstr ""
"\n"
"En-tête étendu : \n"
"\n"

#: tools/gdbmshell.c:1093
#, c-format
msgid "      version = %d\n"
msgstr "      version = %d\n"

#: tools/gdbmshell.c:1094
#, c-format
msgid "      numsync = %u\n"
msgstr "      numsync = %u\n"

#: tools/gdbmshell.c:1208
msgid "bad file mode"
msgstr "mauvais mode de fichier"

#: tools/gdbmshell.c:1218
msgid "insufficient precision"
msgstr "précision insuffisante"

#. TRANSLATORS: Stands for "Not Available".
#: tools/gdbmshell.c:1231
msgid "N/A"
msgstr "N/A"

#: tools/gdbmshell.c:1239 tools/gdbmshell.c:1244
msgid "can't open database"
msgstr "impossible d'ouvrir la base de données"

#: tools/gdbmshell.c:1249
msgid "not a regular file"
msgstr "pas un fichier régulier"

#: tools/gdbmshell.c:1254
msgid "ERROR"
msgstr "ERREUR"

#: tools/gdbmshell.c:1264
#, c-format
msgid "%s: ERROR: can't stat: %s"
msgstr "%s: ERREUR: impossible d'exécuter stat : %s"

#: tools/gdbmshell.c:1288
msgid "Invalid arguments in call to gdbm_latest_snapshot"
msgstr "Arguments invalides dans l'appel à gdbm_latest_snapshot"

#: tools/gdbmshell.c:1293
msgid ""
"Function is not implemented: GDBM is built without crash-tolerance support"
msgstr ""
"La fonction n'est pas implémentée : GDBM est compilé sans support pour la "
"tolérance aux plantages"

#: tools/gdbmshell.c:1301
msgid "Selected the most recent snapshot"
msgstr "L'instantané le plus récent est sélectionné"

#: tools/gdbmshell.c:1305
msgid "Neither snapshot is readable"
msgstr "Aucun des instantané est lisible"

#: tools/gdbmshell.c:1310
msgid "Error selecting snapshot"
msgstr "Erreur en sélectionnant l'instantané"

#: tools/gdbmshell.c:1315
msgid "Snapshot modes and dates are the same"
msgstr "Les modes et les dates de l'instantané sont les mêmes"

#: tools/gdbmshell.c:1320
msgid "Snapshot sync counters differ by more than 1"
msgstr "Le compteur de synchronisation de l'instantané diffère de plus de 1"

#: tools/gdbmshell.c:1348
#, c-format
msgid "unexpected error code: %d"
msgstr "code d'erreur inattendu : %d"

#: tools/gdbmshell.c:1368
#, c-format
msgid "hash value = %x, bucket #%u, slot %u"
msgstr "valeur hachage = %x, compartiment #%u, emplacement %u"

#: tools/gdbmshell.c:1374
#, c-format
msgid "hash value = %x"
msgstr "valeur hachage = %x"

#: tools/gdbmshell.c:1427
#, c-format
msgid "unrecognized parameter: %s\n"
msgstr "paramètre non reconnu: %s\n"

#: tools/gdbmshell.c:1434
msgid "select bucket first\n"
msgstr "sélectionnez le compartiment en premier\n"

#: tools/gdbmshell.c:1485
#, c-format
msgid "error reading entry %d"
msgstr "erreur lors de la lecture de l'entrée %d"

#: tools/gdbmshell.c:1494 tools/gdbmshell.c:1532
msgid "the key was:"
msgstr "la clé était :"

#: tools/gdbmshell.c:1608
msgid "error dumping database"
msgstr "erreur durant la décharge de la base de données"

#: tools/gdbmshell.c:1667
msgid "error restoring metadata"
msgstr "erreur lors de la récupération des méta données"

#: tools/gdbmshell.c:1700
#, c-format
msgid "Database file: %s\n"
msgstr "Ficher base de données: %s\n"

#: tools/gdbmshell.c:1702
msgid "Database is open"
msgstr "La base de données est ouverte"

#: tools/gdbmshell.c:1704
msgid "Database is not open"
msgstr "La base de données n'est pas ouverte"

#: tools/gdbmshell.c:1759
#, c-format
msgid "unknown debug flag: %s"
msgstr "fanion de débogage inconnu: %s"

#: tools/gdbmshell.c:1762
#, c-format
msgid "invalid type of argument %d"
msgstr "type d'argument %d invalide"

#: tools/gdbmshell.c:1767
#, c-format
msgid "Debug flags:"
msgstr "Fanions de débogage:"

#: tools/gdbmshell.c:1773
msgid "none"
msgstr "aucun"

#: tools/gdbmshell.c:1777
msgid "compiled without debug support"
msgstr "compilé sans support pour le débogage"

#: tools/gdbmshell.c:1828
#, c-format
msgid "command failed with status %d"
msgstr "la commande a échoué avec le statut %d"

#: tools/gdbmshell.c:1831
#, c-format
msgid "command terminated on signal %d"
msgstr "la commande a été terminée par le signal %d"

#. TRANSLATORS: %s is the stream name
#: tools/gdbmshell.c:1901
#, c-format
msgid "input history is not available for %s input stream"
msgstr "l'historique d'entrée n'est pas disponible pour le flux d'entrée %s"

#: tools/gdbmshell.c:1994
msgid "count (number of entries)"
msgstr "nombre (nombre d'entrées)"

#: tools/gdbmshell.c:2004 tools/gdbmshell.c:2032 tools/gdbmshell.c:2085
#: tools/gdbmshell.c:2194
msgid "KEY"
msgstr "CLÉ"

#: tools/gdbmshell.c:2007
msgid "delete a record"
msgstr "effacer un enregistrement"

#: tools/gdbmshell.c:2017 tools/gdbmshell.c:2045 tools/gdbmtool.c:93
msgid "FILE"
msgstr "FICHIER"

#: tools/gdbmshell.c:2022
msgid "export"
msgstr "exporter"

#: tools/gdbmshell.c:2035
msgid "fetch record"
msgstr "récupérer un enregistrement"

#: tools/gdbmshell.c:2050
msgid "import"
msgstr "importer"

#: tools/gdbmshell.c:2062
msgid "list"
msgstr "lister"

#: tools/gdbmshell.c:2072
msgid "[KEY]"
msgstr "[CLÉ]"

#: tools/gdbmshell.c:2075
msgid "continue iteration: get next key and datum"
msgstr "continuer l'itération : obtenir la clé et la donnée suivante"

#: tools/gdbmshell.c:2086
msgid "DATA"
msgstr "DONNÉE"

#: tools/gdbmshell.c:2089
msgid "store"
msgstr "stockage"

#: tools/gdbmshell.c:2098
msgid "begin iteration: get first key and datum"
msgstr "commencer l'itération : obtenir la première clé et la première donnée"

#: tools/gdbmshell.c:2107
msgid "reorganize"
msgstr "réorganiser"

#: tools/gdbmshell.c:2126
msgid "recover the database"
msgstr "récupérer la base de données"

#: tools/gdbmshell.c:2135
msgid "print avail list"
msgstr "afficher la liste des emplacements libres"

#: tools/gdbmshell.c:2145
msgid "[NUMBER]"
msgstr "[NOMBRE]"

#: tools/gdbmshell.c:2148
msgid "print a bucket"
msgstr "afficher un compartiment"

#: tools/gdbmshell.c:2157
msgid "print current bucket"
msgstr "afficher le compartiment courant"

#: tools/gdbmshell.c:2166
msgid "print sibling bucket"
msgstr "afficher le compartiment frère"

#: tools/gdbmshell.c:2175
msgid "print hash directory"
msgstr "afficher le répertoire de hachage"

#: tools/gdbmshell.c:2184
msgid "print database file header"
msgstr "afficher l'en-tête du fichier de base de données"

#: tools/gdbmshell.c:2197
msgid "hash value of key"
msgstr "valeur de hachage de la clé"

#: tools/gdbmshell.c:2205
msgid "print the bucket cache"
msgstr "afficher la cache compartimentée"

#: tools/gdbmshell.c:2214
msgid "print current program status"
msgstr "afficher le statut actuel du programme"

#: tools/gdbmshell.c:2222
msgid "Synchronize the database with disk copy"
msgstr "Synchroniser la base de données avec la copie sur disque"

#: tools/gdbmshell.c:2231
msgid "Upgrade the database to extended format"
msgstr "Mettre à jour la base de données vers le format étendu"

#: tools/gdbmshell.c:2240
msgid "Downgrade the database to standard format"
msgstr "Revenir en arrière vers la base de données avec le format standard"

#: tools/gdbmshell.c:2254
msgid "analyze two database snapshots"
msgstr "analyser deux instantanés de la base de données"

#: tools/gdbmshell.c:2262
msgid "print version of gdbm"
msgstr "afficher la version de gdbm"

#: tools/gdbmshell.c:2270
msgid "print this help list"
msgstr "afficher cette liste d'aide"

#: tools/gdbmshell.c:2279
msgid "quit the program"
msgstr "quitter le programme"

#: tools/gdbmshell.c:2291
msgid "set or list variables"
msgstr "changer ou afficher les variables"

#: tools/gdbmshell.c:2302
msgid "unset variables"
msgstr "supprimer des variables"

#: tools/gdbmshell.c:2314
msgid "define datum structure"
msgstr "définir la structure de données"

#: tools/gdbmshell.c:2325
msgid "source command script"
msgstr "script de commande source"

#: tools/gdbmshell.c:2333
msgid "close the database"
msgstr "fermer la base de données"

#: tools/gdbmshell.c:2345
msgid "open new database"
msgstr "ouvrir une nouvelle base de données"

#: tools/gdbmshell.c:2354
msgid "[FROM]"
msgstr "[DE]"

#: tools/gdbmshell.c:2355
msgid "[COUNT]"
msgstr "[NOMBRE]"

#: tools/gdbmshell.c:2358
msgid "show input history"
msgstr "montrer l'historique d'entrée"

#: tools/gdbmshell.c:2367
msgid "query/set debug level"
msgstr "interroger/définir le niveau de débogage"

#: tools/gdbmshell.c:2386
msgid "invoke the shell"
msgstr "invoquer l'interpréteur de commande"

#: tools/gdbmshell.c:2398
msgid "describe GDBM error code"
msgstr "décrire le code d'erreur de GDBM"

#: tools/gdbmshell.c:2547
msgid "Invalid command. Try ? for help."
msgstr "Commande invalide. Essayez ? pour l'aide."

#: tools/gdbmshell.c:2548
msgid "Unknown command"
msgstr "Commande inconnue"

#: tools/gdbmshell.c:2863
#, c-format
msgid "cannot coerce %s to %s"
msgstr "ne peut contraindre %s en %s"

#: tools/gdbmshell.c:3044
#, c-format
msgid "%s: not enough arguments"
msgstr "%s: pas assez d'arguments"

#: tools/gdbmshell.c:3052
msgid "unexpected eof"
msgstr "fin de fichier inattendue"

#: tools/gdbmshell.c:3068
#, c-format
msgid "%s: too many arguments"
msgstr "%s: trop d'arguments"

#: tools/gdbmshell.c:3129
#, c-format
msgid "cannot run pager `%s': %s"
msgstr "le pager « %s » ne démarre pas: %s"

#: tools/gdbmshell.c:3234
#, c-format
msgid ""
"\n"
"Welcome to the gdbm tool.  Type ? for help.\n"
"\n"
msgstr ""
"\n"
"Bienvenu dans l'outil gdbm. Tapez ? pour l'aide.\n"
"\n"

#: tools/gdbmtool.c:48
msgid "cannot find home directory"
msgstr "ne peut trouver le dossier personnel"

#: tools/gdbmtool.c:82
msgid "examine and/or modify a GDBM database"
msgstr "examiner ou modifier une base de données GDBM"

#: tools/gdbmtool.c:83
msgid "DBFILE [COMMAND [ARG ...]]"
msgstr "DBFILE [COMMANDE [ARG ...]]"

#: tools/gdbmtool.c:91 tools/gdbmtool.c:92
msgid "SIZE"
msgstr "TAILLE"

#: tools/gdbmtool.c:91
msgid "set block size"
msgstr "changer la taille de bloc"

#: tools/gdbmtool.c:92
msgid "set cache size"
msgstr "changer la taille de la cache"

#: tools/gdbmtool.c:93
msgid "read commands from FILE"
msgstr "lire les commandes depuis FICHIER"

#: tools/gdbmtool.c:95
msgid "disable file locking"
msgstr "désactiver le verrouillage du fichier"

#: tools/gdbmtool.c:96
msgid "do not use mmap"
msgstr "ne pas utiliser mmap"

#: tools/gdbmtool.c:97
msgid "create database"
msgstr "créer une base de données"

#: tools/gdbmtool.c:98
msgid "do not read .gdbmtoolrc file"
msgstr "ne pas lire le fichier .gdbmtoolrc"

#: tools/gdbmtool.c:99
msgid "open database in read-only mode"
msgstr "ouvrir la base de données en lecture seule"

#: tools/gdbmtool.c:100
msgid "synchronize to disk after each write"
msgstr "synchroniser sur le disque après chaque écriture"

#: tools/gdbmtool.c:101
msgid "don't print initial banner"
msgstr "ne pas imprimer la banderole initiale"

#. TRANSLATORS: File Descriptor.
#: tools/gdbmtool.c:104
msgid "FD"
msgstr "DdF"

#: tools/gdbmtool.c:105
msgid "open database at the given file descriptor"
msgstr "ouvrir la base de données au descripteur de fichier donné"

#: tools/gdbmtool.c:106
msgid "extended format (numsync)"
msgstr "format étendu (numsync)"

#: tools/gdbmtool.c:108
msgid "enable trace mode"
msgstr "activer le mode de traçage"

#: tools/gdbmtool.c:109
msgid "print timing after each command"
msgstr "afficher le temps après chaque commande"

#: tools/gdbmtool.c:111
msgid "enable lexical analyzer traces"
msgstr "activer les traces de l'analyseur lexical"

#: tools/gdbmtool.c:112
msgid "enable grammar traces"
msgstr "activer les traces de la grammaire"

#: tools/gdbmtool.c:149
#, c-format
msgid "invalid file descriptor: %s"
msgstr "descripteur de fichier invalide : %s"

#: tools/gdbmtool.c:226
#, c-format
msgid "unknown option %s; try `%s -h' for more info"
msgstr ""
"option %s inconnue ; essayez « %s -h » pour obtenir plus d'informations"

#: tools/gdbmtool.c:229
#, c-format
msgid "unknown option %c; try `%s -h' for more info"
msgstr "option %c inconnue; essayez « %s -h » pour obtenir plus d'informations"

#: tools/gdbmtool.c:253
msgid "--file and command cannot be used together"
msgstr "--file et la commande ne peuvent pas être utilisés ensembles"

#: tools/gram.y:179
#, c-format
msgid "duplicate tag: %s"
msgstr "étiquette dupliquée : %s"

#: tools/gram.y:261
#, c-format
msgid "expected \"key\" or \"content\", but found \"%s\""
msgstr "« key » ou « content » attendu, mais « %s » trouvé"

#: tools/gram.y:333 tools/gram.y:362 tools/gram.y:394
#, c-format
msgid "no such variable: %s"
msgstr "pas de telle variable: %s"

#: tools/gram.y:337
#, c-format
msgid "%s is not a boolean variable"
msgstr "%s n'est pas une variable booléenne"

#: tools/gram.y:341
#, c-format
msgid "%s: setting is not allowed"
msgstr "%s: le réglage n'est pas permis"

#: tools/gram.y:345
msgid "can't set variable"
msgstr "impossible de définir la variable"

#: tools/gram.y:349 tools/gram.y:374
#, c-format
msgid "unexpected error setting %s: %d"
msgstr "erreur inattendue en changeant %s: %d"

#: tools/gram.y:366
#, c-format
msgid "%s: bad variable type"
msgstr "%s: mauvais type de variable"

#: tools/gram.y:370
#, c-format
msgid "%s: value %s is not allowed"
msgstr "%s: la valeur %s n'est pas permise"

#: tools/gram.y:398
#, c-format
msgid "%s: variable cannot be unset"
msgstr "%s: la variable ne peut pas être supprimée"

#: tools/input-file.c:60
#, c-format
msgid "cannot open `%s': %s"
msgstr "ne peut ouvrir « %s »: %s"

#: tools/input-file.c:65
#, c-format
msgid "%s is not a regular file"
msgstr "%s n'est pas un fichier régulier"

#: tools/input-file.c:72
#, c-format
msgid "cannot open %s for reading: %s"
msgstr "impossible d'ouvrir %s en lecture: %s"

#: tools/lex.l:146
msgid "recursive sourcing"
msgstr "source récursive"

#: tools/lex.l:148
#, c-format
msgid "%s already sourced here"
msgstr "%s a déjà été incorporé ici"

#: tools/lex.l:259 tools/lex.l:269
msgid "invalid #line statement"
msgstr "instruction #line invalide"

#: tools/parseopt.c:49
msgid "give this help list"
msgstr "afficher cette liste d'aide"

#: tools/parseopt.c:50
msgid "print program version"
msgstr "afficher la version du programme"

#: tools/parseopt.c:51
msgid "give a short usage message"
msgstr "afficher un court message d'utilisation"

#: tools/parseopt.c:289
#, c-format
msgid "error in ARGP_HELP_FMT: improper usage of [no-]%s\n"
msgstr "erreur dans ARGP_HELP_FMT : utilisation incorrecte de [no-]%s\n"

#: tools/parseopt.c:307
#, c-format
msgid "error in ARGP_HELP_FMT: bad value for %s"
msgstr "erreur de ARGP_HELP_FMT : mauvaise valeur pour %s"

#: tools/parseopt.c:311
#, c-format
msgid " (near %s)"
msgstr " (près de %s)"

#: tools/parseopt.c:320
#, c-format
msgid "error in ARGP_HELP_FMT: %s value is out of range\n"
msgstr "erreur dans ARGP_HELP_FMT : la valeur %s est hors limites\n"

#: tools/parseopt.c:331
#, c-format
msgid "%s: ARGP_HELP_FMT parameter requires a value\n"
msgstr "%s: le paramètre ARGP_HELP_FMT requiert une valeur\n"

#: tools/parseopt.c:340
#, c-format
msgid "%s: Unknown ARGP_HELP_FMT parameter\n"
msgstr "%s: Paramètre ARGP_HELP_FMT inconnu\n"

#: tools/parseopt.c:367
#, c-format
msgid "ARGP_HELP_FMT: missing delimiter near %s\n"
msgstr "ARGP_HELP_FMT: délimiteur manquant près de %s\n"

#: tools/parseopt.c:487 tools/parseopt.c:565
msgid "Usage:"
msgstr "Utilisation:"

#: tools/parseopt.c:489
msgid "OPTION"
msgstr "OPTION"

#: tools/parseopt.c:510
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Les arguments obligatoires ou facultatifs des options longues sont également "
"obligatoires ou facultatifs pour l'option courte correspondante."

#. TRANSLATORS: The placeholder indicates the bug-reporting address
#. for this package.  Please add _another line_ saying
#. "Report translation bugs to <...>\n" with the address for translation
#. bugs (typically your translation team's web or email address).
#: tools/parseopt.c:523
#, c-format
msgid "Report bugs to %s.\n"
msgstr ""
"Signalez les bugs à %s.\n"
"Signalez les bugs de traduction a traduc@traduc.org.\n"

#: tools/parseopt.c:526
#, c-format
msgid "%s home page: <%s>\n"
msgstr "page d'accueil de %s: <%s>\n"

#. TRANSLATORS: Translate "(C)" to the copyright symbol
#. (C-in-a-circle), if this symbol is available in the user's
#. locale.  Otherwise, do not translate "(C)"; leave it as-is.
#: tools/parseopt.c:667
msgid "(C)"
msgstr "©"

#. TRANSLATORS: Please, don't translate 'y' and 'n'.
#: tools/util.c:100
msgid "Please, reply 'y' or 'n'"
msgstr "Merci de répondre « y » ou « n »"

#: tools/var.c:792
#, c-format
msgid "unrecognized error code: %s"
msgstr "code d'erreur non reconnu : %s"

#~ msgid ""
#~ "bits = %d\n"
#~ "count= %d\n"
#~ "Hash Table:\n"
#~ msgstr ""
#~ "bits = %d\n"
#~ "nombre= %d\n"
#~ "Table de hachage:\n"

#~ msgid "Current bucket"
#~ msgstr "Compartiment actuel"

#~ msgid " current bucket address  = %lu.\n"
#~ msgstr " adresse du compartiment courant = %lu.\n"

#~ msgid "  table        = %lu\n"
#~ msgstr "  table        = %lu\n"

#~ msgid "  table size   = %d\n"
#~ msgstr "  taille table = %d\n"

#~ msgid "  table bits   = %d\n"
#~ msgstr "  bits table   = %d\n"

#~ msgid "Illegal data"
#~ msgstr "Donnée illégale"

#~ msgid "Illegal option"
#~ msgstr "Option illégale"

#~ msgid "warning: using default database file %s"
#~ msgstr "attention: utilisation du fichier de base de données par défaut %s"

#~ msgid "Not a bucket."
#~ msgstr "Pas un compartiment."

#~ msgid "cannot load from %s: %s"
#~ msgstr "ne peut charger depuis %s: %s"

#~ msgid "No database name"
#~ msgstr "Pas de nom de base de données"

#~ msgid ""
#~ "\n"
#~ "header block\n"
#~ "size  = %d\n"
#~ "count = %d\n"
#~ msgstr ""
#~ "\n"
#~ "bloc d'en-tête\n"
#~ "taille = %d\n"
#~ "nombre = %d\n"

#~ msgid "couldn't init cache"
#~ msgstr "La cache n'a pas pu être initialisée"

#~ msgid "invalid avail_block"
#~ msgstr "avail_block invalide"

#~ msgid "nextkey"
#~ msgstr "nextkey"

#~ msgid "firstkey"
#~ msgstr "firstkey"

#~ msgid "Wrong file offset"
#~ msgstr "Mauvais décalage dans le fichier"

#~ msgid "cannot fetch data; the key was:"
#~ msgstr "ne peut récupérer la donnée. La clé était:"

#~ msgid "too many arguments"
#~ msgstr "trop d'arguments"

#~ msgid "Unknown update"
#~ msgstr "Mise à jour inconnue"

#~ msgid "Unknown error"
#~ msgstr "Erreur inconnue"

#~ msgid "gdbm fatal: %s\n"
#~ msgstr "erreur fatale gdbm: %s\n"

#~ msgid "Out of memory"
#~ msgstr "À cours de mémoire"

#~ msgid "Usage: %s OPTIONS\n"
#~ msgstr "Usage: %s OPTIONS\n"

#~ msgid "  -b SIZE            set block size\n"
#~ msgstr "  -b TAILLE          fixer la taille des blocs\n"

#~ msgid "  -c SIZE            set cache size\n"
#~ msgstr "  -c TAILLE          fixez la taille de la cache\n"

#~ msgid "  -g FILE            operate on FILE instead of `junk.gdbm'\n"
#~ msgstr "  -g FICHIER         agir sur FICHIER au lieu de « junk.gdbm »\n"

#~ msgid "  -h                 print this help summary\n"
#~ msgstr "  -h                 afficher cette aide\n"

#~ msgid "  -m                 disable file mmap\n"
#~ msgstr "  -m                 désactiver mmap sur fichier\n"

#~ msgid "  -n                 create database\n"
#~ msgstr "  -n                 créer la base de données\n"

#~ msgid "%s:%d: line too long"
#~ msgstr "%s:%d: ligne trop longue"

#~ msgid "%s:%d: malformed line"
#~ msgstr "%s:%d: ligne mal formée"

#~ msgid "%d: item not inserted: %s"
#~ msgstr "%d: élément pas inséré: %s"

#~ msgid "gdbm_export failed, %s"
#~ msgstr "gdbm_export a échoué, %s"

#~ msgid "gdbm_import failed, %s"
#~ msgstr "gdbm_import a échoué, %s"

#~ msgid "yes"
#~ msgstr "oui"

#~ msgid "no"
#~ msgstr "non"

#~ msgid "Zero terminated keys: %s\n"
#~ msgstr "Clés terminées par un zéro: %s\n"

#~ msgid "key"
#~ msgstr "clé"

#~ msgid "delete"
#~ msgstr "supprimer"

#~ msgid "file"
#~ msgstr "fichier"

#~ msgid "fetch"
#~ msgstr "récupérer"

#~ msgid "[key]"
#~ msgstr "[clé]"

#~ msgid "data"
#~ msgstr "donnée"

#~ msgid "read entries from file and store"
#~ msgstr "lis les entrées dans le fichier et les stocke"

#~ msgid "toggle key nul-termination"
#~ msgstr "(dés)activer la terminaison des clés par un zéro"

#~ msgid "bucket-number"
#~ msgstr "numéro compartiment"

#~ msgid "toggle data nul-termination"
#~ msgstr "(dés)activer la terminaison des données par un zéro"

#~ msgid "-s is incompatible with -r"
#~ msgstr "-s est incompatible avec -r"

#~ msgid "-n is incompatible with -r"
#~ msgstr "-n est incompatible avec -r"
