# Russian translation for gdbm.
#
# Copyright (C) 2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the gdbm package.
#
# Yuri Kozlov <yuray@komyakino.ru>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: gdbm 1.23\n"
"Report-Msgid-Bugs-To: bug-gdbm@gnu.org\n"
"POT-Creation-Date: 2024-07-01 09:33+0300\n"
"PO-Revision-Date: 2022-02-19 11:02+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <gnu@d07.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#: src/bucket.c:399 src/bucket.c:685 src/falloc.c:211 src/falloc.c:337
#: src/findkey.c:124 src/gdbmstore.c:168 src/update.c:36 src/update.c:76
msgid "lseek error"
msgstr "ошибка lseek"

#: src/bucket.c:534
msgid "directory overflow"
msgstr "переполнение каталога"

#: src/bucket.c:545 src/falloc.c:302 src/findkey.c:97 src/findkey.c:113
msgid "malloc error"
msgstr "ошибка malloc"

#: src/falloc.c:201
msgid "malloc failed"
msgstr "вызов malloc завершился ошибкой"

#: src/gdbmerrno.c:102
msgid "No error"
msgstr "Нет ошибок"

#: src/gdbmerrno.c:103
msgid "Malloc error"
msgstr "Ошибка malloc"

#: src/gdbmerrno.c:104
msgid "Block size error"
msgstr "Ошибка в размере блока"

#: src/gdbmerrno.c:105
msgid "File open error"
msgstr "Ошибка открытия файла"

#: src/gdbmerrno.c:106
msgid "File write error"
msgstr "Ошибка записи в файл"

#: src/gdbmerrno.c:107
msgid "File seek error"
msgstr "Ошибка перемещения по файлу"

#: src/gdbmerrno.c:108
msgid "File read error"
msgstr "Ошибка чтения файла"

#: src/gdbmerrno.c:109
msgid "Bad magic number"
msgstr "Некорректный опознавательный номер"

#: src/gdbmerrno.c:110
msgid "Empty database"
msgstr "Пустая база данных"

#: src/gdbmerrno.c:111
msgid "Can't be reader"
msgstr "Невозможно быть читающей стороной"

#: src/gdbmerrno.c:112
msgid "Can't be writer"
msgstr "Невозможно быть пишущей стороной"

#: src/gdbmerrno.c:113
msgid "Reader can't delete"
msgstr "Пишущая сторона не может удалять"

#: src/gdbmerrno.c:114
msgid "Reader can't store"
msgstr "Пишущая сторона не может сохранять"

#: src/gdbmerrno.c:115
msgid "Reader can't reorganize"
msgstr "Пишущая сторона не может выполнять перекомпоновку"

#: src/gdbmerrno.c:116
msgid "Should not happen: unused error code"
msgstr "Этого не должно случиться: неиспользуемый код ошибки"

#: src/gdbmerrno.c:117
msgid "Item not found"
msgstr "Элемент не найден"

#: src/gdbmerrno.c:118
msgid "Reorganize failed"
msgstr "Ошибка перекомпоновки"

#: src/gdbmerrno.c:119
msgid "Cannot replace"
msgstr "Замена невозможна"

#: src/gdbmerrno.c:120
msgid "Malformed data"
msgstr "Искажённые данные"

#: src/gdbmerrno.c:121
msgid "Option already set"
msgstr "Параметр уже задан"

#: src/gdbmerrno.c:122
msgid "Bad option value"
msgstr "Некорректное значение параметра"

#: src/gdbmerrno.c:123
msgid "Byte-swapped file"
msgstr "Файл с переставленными байтами"

#: src/gdbmerrno.c:124
msgid "File header assumes wrong off_t size"
msgstr "В файловом заголовке указан неправильный размер off_t"

#: src/gdbmerrno.c:125
msgid "Bad file flags"
msgstr "Неверные флаги файла"

#: src/gdbmerrno.c:126
msgid "Cannot stat file"
msgstr "Для файла не удалось выполнить функцию stat"

#: src/gdbmerrno.c:127
msgid "Unexpected end of file"
msgstr "Неожиданный конец файла"

#: src/gdbmerrno.c:128
msgid "Database name not given"
msgstr "Не задано имя базы данных"

#: src/gdbmerrno.c:129
msgid "Failed to restore file owner"
msgstr "Сбой при восстановлении владельца файла"

#: src/gdbmerrno.c:130
msgid "Failed to restore file mode"
msgstr "Сбой при восстановлении режима доступа к файлу"

#: src/gdbmerrno.c:131
msgid "Database needs recovery"
msgstr "База данных нуждается в восстановлении"

#: src/gdbmerrno.c:132
msgid "Failed to create backup copy"
msgstr "Не удалось создать резервную копию"

#: src/gdbmerrno.c:133
msgid "Bucket directory overflow"
msgstr "Переполнение корзины каталогов"

#: src/gdbmerrno.c:134
msgid "Malformed bucket header"
msgstr "Неправильный формат заголовка корзины"

#: src/gdbmerrno.c:135
msgid "Malformed database file header"
msgstr "Неправильный формат заголовка файла базы данных"

#. TRANSLATORS: avail_block is a field name. Don't translate it.
#: src/gdbmerrno.c:137
msgid "Malformed avail_block"
msgstr "Неправильный avail_block"

#: src/gdbmerrno.c:138
msgid "Malformed hash table"
msgstr "Неправильная хэш-таблица"

#: src/gdbmerrno.c:139
msgid "Invalid directory entry"
msgstr "Неправильный элемент каталога"

#: src/gdbmerrno.c:140
msgid "Error closing file"
msgstr "Ошибка закрытия файла"

#: src/gdbmerrno.c:141
msgid "Error synchronizing file"
msgstr "Ошибка синхронизации файла"

#: src/gdbmerrno.c:142
msgid "Error truncating file"
msgstr "Ошибка усечения файла"

#: src/gdbmerrno.c:143
msgid "Bucket cache corrupted"
msgstr "Кэш корзины повреждён"

#: src/gdbmerrno.c:144
msgid "Malformed bucket hash entry"
msgstr "Искажённый элемент хэша корзины"

#: src/gdbmerrno.c:145
msgid "Reflink failed"
msgstr "Ошибка reflink"

#: src/gdbmerrno.c:146
msgid "Failed to resolve real path name"
msgstr "Не удалось определить действительное имя пути"

#: src/gdbmerrno.c:147
msgid "Function usage error"
msgstr "Ошибка использования функции"

#: src/recover.c:272
#, c-format
msgid "can't read bucket #%d: %s"
msgstr "невозможно прочитать корзину #%d: %s"

#: src/recover.c:300
#, c-format
msgid "can't read key pair %d:%d (%lu:%d): %s"
msgstr "невозможно прочитать пару ключей %d:%d (%lu:%d): %s"

#: src/recover.c:330
#, c-format
msgid "ignoring duplicate key %d:%d (%lu:%d)"
msgstr "игнорируется повторяющийся ключ %d:%d (%lu:%d)"

#: src/recover.c:340
#, c-format
msgid "fatal: can't store element %d:%d (%lu:%d): %s"
msgstr "критическая ошибка: невозможно сохранить элемент %d:%d (%lu:%d): %s"

#: tools/datconv.c:291
#, c-format
msgid "(not enough data)"
msgstr "(недостаточно данных)"

#: tools/datconv.c:383
msgid "cannot convert"
msgstr "невозможно преобразовать"

#: tools/datconv.c:392
#, c-format
msgid "cannot convert value #%d: %s"
msgstr "невозможно преобразовать значение #%d: %s"

#: tools/datconv.c:418 tools/datconv.c:467
msgid "mixing tagged and untagged values is not allowed"
msgstr "нельзя использовать значения с тегом и без тега одновременно"

#: tools/datconv.c:472
#, c-format
msgid "%s: no such field in datum"
msgstr "%s: отсутствует поле в datum"

#: tools/gdbm_dump.c:22
msgid "dump a GDBM database to a file"
msgstr "сделать дамп базы данных GDBM в файл"

#: tools/gdbm_dump.c:23
msgid "DB_FILE [FILE]"
msgstr "ФАЙЛ_БД [ФАЙЛ]"

#: tools/gdbm_dump.c:25
msgid "select dump format"
msgstr "формат дампа"

#: tools/gdbm_dump.c:67 tools/gdbm_dump.c:79
msgid "unknown dump format"
msgstr "неизвестный формат дампа"

#: tools/gdbm_dump.c:86 tools/gdbm_load.c:225
msgid "unknown option"
msgstr "неизвестный параметр"

#: tools/gdbm_dump.c:102 tools/gdbm_load.c:241
#, c-format
msgid "too many arguments; try `%s -h' for more info"
msgstr "слишком много аргументов; посмотрите справку «%s -h»"

#: tools/gdbm_dump.c:122 tools/gdbm_load.c:267
#, c-format
msgid "cannot open %s"
msgstr "не удалось открыть %s"

#: tools/gdbm_dump.c:130 tools/gdbm_load.c:277
msgid "gdbm_open failed"
msgstr "вызов gdbm_open завершился ошибкой"

#: tools/gdbm_dump.c:137
#, fuzzy, c-format
#| msgid "dump error"
msgid "%s: dump error"
msgstr "ошибка создания дампа"

#: tools/gdbm_load.c:32
msgid "load a GDBM database from a file"
msgstr "загрузить базу данных GDBM из файла"

#: tools/gdbm_load.c:33
msgid "FILE [DB_FILE]"
msgstr "ФАЙЛ [ФАЙЛ_БД]"

#: tools/gdbm_load.c:35
#, fuzzy
#| msgid "replace records in the existing database"
msgid "replace records in the existing database (needs -U)"
msgstr "заменять записи в существующей базе данных"

#: tools/gdbm_load.c:36
msgid "MODE"
msgstr "РЕЖИМ"

#: tools/gdbm_load.c:36
msgid "set file mode"
msgstr "режим доступа к файлу"

#: tools/gdbm_load.c:37
#, fuzzy
#| msgid "replace records in the existing database"
msgid "update the existing database"
msgstr "заменять записи в существующей базе данных"

#: tools/gdbm_load.c:38
msgid "NAME|UID[:NAME|GID]"
msgstr "ИМЯ|UID[:ИМЯ|GID]"

#: tools/gdbm_load.c:38
msgid "set file owner"
msgstr "владелец файла"

#: tools/gdbm_load.c:39
msgid "do not attempt to set file meta-data"
msgstr "не пытаться задать метаданные файла"

#: tools/gdbm_load.c:40
msgid "use memory mapping"
msgstr "использовать проецирование в память"

#: tools/gdbm_load.c:41 tools/gdbm_load.c:42
msgid "NUM"
msgstr "ЧИСЛО"

#: tools/gdbm_load.c:41
msgid "set the cache size"
msgstr "размер кэша"

#: tools/gdbm_load.c:42
msgid "set the block size"
msgstr "размер блока"

#: tools/gdbm_load.c:80
#, c-format
msgid "invalid number: %s"
msgstr "неверное число: %s"

#: tools/gdbm_load.c:85
#, c-format
msgid "invalid number: %s: %s"
msgstr "неверное число: %s %s"

#: tools/gdbm_load.c:137
#, c-format
msgid "invalid octal number"
msgstr "неверное восьмеричное число"

#: tools/gdbm_load.c:167
#, c-format
msgid "invalid user name: %s"
msgstr "неверное имя пользователя: %s"

#: tools/gdbm_load.c:186
#, c-format
msgid "invalid group name: %s"
msgstr "неверное имя группы: %s"

#: tools/gdbm_load.c:198
#, c-format
msgid "no such UID: %lu"
msgstr "UID не найден: %lu"

#: tools/gdbm_load.c:247
msgid "-r is useless without -U"
msgstr ""

#: tools/gdbm_load.c:283
#, c-format
msgid "gdbm_setopt failed: %s"
msgstr "вызов gdbm_setopt завершился ошибкой: %s"

#: tools/gdbm_load.c:297 tools/gdbm_load.c:315
#, c-format
msgid "error restoring metadata: %s (%s)"
msgstr "ошибка восстановления метаданных: %s (%s)"

#: tools/gdbm_load.c:306 tools/gdbmshell.c:1674
#, c-format
msgid "cannot load from %s"
msgstr "невозможно загрузить из %s"

#: tools/gdbm_load.c:323
msgid "gdbm_setopt failed"
msgstr "вызов gdbm_setopt завершился ошибкой"

#: tools/gdbmshell.c:64 tools/gdbmshell.c:157 tools/gdbmshell.c:361
#: tools/gdbmshell.c:497 tools/gdbmshell.c:905 tools/gdbmshell.c:942
#: tools/gdbmshell.c:951
#, c-format
msgid "%s failed"
msgstr "%s завершился ошибкой"

#: tools/gdbmshell.c:120
#, c-format
msgid "database %s already exists; overwrite"
msgstr "база данных %s уже существует; перезаписываем"

#: tools/gdbmshell.c:151
#, c-format
msgid "cannot open database %s"
msgstr "не удалось открыть базу данных %s"

#: tools/gdbmshell.c:252
#, c-format
msgid "Bucket #%d"
msgstr "Корзина #%d"

#: tools/gdbmshell.c:255
#, c-format
msgid ""
"address     = %lu\n"
"depth       = %d\n"
"hash prefix = %08x\n"
"references  = %u"
msgstr ""
"адрес       = %lu\n"
"глубина     = %d\n"
"хэш-префикс = %08x\n"
"ссылок      = %u"

#: tools/gdbmshell.c:270
#, c-format
msgid ""
"count       = %d\n"
"load factor = %3d\n"
msgstr ""
"количество  = %d\n"
"коэффициент использования  = %3d\n"

#: tools/gdbmshell.c:275
msgid "Hash Table:\n"
msgstr "Хэш-таблица:\n"

#: tools/gdbmshell.c:277
#, c-format
msgid ""
"    #    hash value     key size    data size     data adr home  key start\n"
msgstr ""
"    #    знач. хэша  разм. ключа  разм. данных  нач. адр. данных  нач. "
"ключа\n"

#: tools/gdbmshell.c:295
#, c-format
msgid ""
"\n"
"Avail count = %d\n"
msgstr ""
"\n"
"Доступное количество = %d\n"

#: tools/gdbmshell.c:296
#, c-format
msgid "Address           size\n"
msgstr "Адрес           размер\n"

#: tools/gdbmshell.c:347
msgid "header block"
msgstr "блок заголовка"

#: tools/gdbmshell.c:349
#, c-format
msgid "block = %lu"
msgstr "блок = %lu"

#: tools/gdbmshell.c:350
#, c-format
msgid ""
"\n"
"size  = %d\n"
"count = %d\n"
msgstr ""
"\n"
"размер  = %d\n"
"количество = %d\n"

#: tools/gdbmshell.c:374
#, c-format
msgid ""
"Bucket Cache (size %zu/%zu):\n"
"  Index:         Address  Changed  Data_Hash \n"
msgstr ""
"Кэш корзины (размер %zu/%zu):\n"
"  Индекс:        Адрес     Изменён Data_Hash \n"

#: tools/gdbmshell.c:381
msgid "True"
msgstr "True"

#: tools/gdbmshell.c:381
msgid "False"
msgstr "False"

#: tools/gdbmshell.c:386
#, c-format
msgid "Bucket cache is empty.\n"
msgstr "Кэш корзины пуст.\n"

#: tools/gdbmshell.c:463
msgid "nothing to close"
msgstr "нечего закрывать"

#: tools/gdbmshell.c:506
msgid "count buffer overflow"
msgstr "переполнение счётчика буфера"

#: tools/gdbmshell.c:509
#, c-format
msgid "There is %s item in the database.\n"
msgid_plural "There are %s items in the database.\n"
msgstr[0] "В базе данных %s запись.\n"
msgstr[1] "В базе данных %s записи.\n"
msgstr[2] "В базе данных %s записей.\n"

#: tools/gdbmshell.c:526 tools/gdbmshell.c:550 tools/gdbmshell.c:630
msgid "No such item found"
msgstr "Элемент не найден"

#: tools/gdbmshell.c:529
msgid "Can't delete"
msgstr "Невозможно удалить"

#: tools/gdbmshell.c:553
msgid "Can't fetch data"
msgstr "Невозможно получить данные"

#: tools/gdbmshell.c:566
msgid "Item not inserted"
msgstr "Элемент не вставлен"

#: tools/gdbmshell.c:594
#, c-format
msgid "No such item found.\n"
msgstr "Элемент не найден.\n"

#: tools/gdbmshell.c:597
msgid "Can't find first key"
msgstr "Невозможно найти первый ключ"

#: tools/gdbmshell.c:634
msgid "Can't find next key"
msgstr "Невозможно найти следующий ключ"

#: tools/gdbmshell.c:645
msgid "Reorganization failed"
msgstr "Ошибка перекомпоновки"

#: tools/gdbmshell.c:649
msgid "Reorganization succeeded."
msgstr "Перекомпоновка выполнена."

#: tools/gdbmshell.c:702 tools/gdbmshell.c:751 tools/gdbmshell.c:1599
#: tools/gdbmshell.c:1635
#, c-format
msgid "unrecognized argument: %s"
msgstr "нераспознанный аргумент: %s"

#: tools/gdbmshell.c:710
#, fuzzy, c-format
#| msgid "%s: bad variable type"
msgid "%s: bad argument type"
msgstr "%s: неправильный тип переменной"

#: tools/gdbmshell.c:715
msgid "unexpected compound statement"
msgstr ""

#: tools/gdbmshell.c:724 tools/gdbmshell.c:734 tools/gdbmshell.c:744
#: tools/gdbmshell.c:856 tools/gdbmshell.c:865
#, c-format
msgid "not a number (stopped near %s)"
msgstr "не число (остановка рядом с %s)"

#: tools/gdbmshell.c:757
#, fuzzy
#| msgid "unexpected eof"
msgid "unexpected datum"
msgstr "неожиданный конец файла"

#: tools/gdbmshell.c:767
#, c-format
msgid "Recovery succeeded.\n"
msgstr "Восстановление выполнено.\n"

#: tools/gdbmshell.c:771
#, c-format
msgid "Keys recovered: %lu, failed: %lu, duplicate: %lu\n"
msgstr "Восстановлено ключей: %lu, не удалось: %lu, дубликатов: %lu\n"

#: tools/gdbmshell.c:776
#, c-format
msgid "Buckets recovered: %lu, failed: %lu\n"
msgstr "Восстановлено корзин: %lu, не удалось: %lu\n"

#: tools/gdbmshell.c:784
#, c-format
msgid "Original database preserved in file %s"
msgstr "Первоначальная база данных сохранена в файле %s"

#: tools/gdbmshell.c:792
msgid "Recovery failed"
msgstr "Сбой при восстановлении"

#: tools/gdbmshell.c:843 tools/gdbmshell.c:926
#, c-format
msgid "no current bucket\n"
msgstr "нет текущей корзины\n"

#: tools/gdbmshell.c:893
#, c-format
msgid "bucket number out of range (0..%lu)"
msgstr "номер корзины выходит за допустимые границы (0..%lu)"

#: tools/gdbmshell.c:936 tools/gdbmshell.c:948
#, c-format
msgid "no sibling\n"
msgstr "не одноуровневых элементов\n"

#: tools/gdbmshell.c:996
#, c-format
msgid "Hash table directory.\n"
msgstr "Каталог хэш-таблицы.\n"

#: tools/gdbmshell.c:997
#, c-format
msgid ""
"  Size =  %d.  Capacity = %lu.  Bits = %d,  Buckets = %zu.\n"
"\n"
msgstr ""
"  Размер =  %d. Ёмкость = %lu. Бит = %d, Корзин = %zu.\n"
"\n"

#: tools/gdbmshell.c:1004
msgid "Index"
msgstr "Индекс"

#: tools/gdbmshell.c:1004
msgid "Hash Pfx"
msgstr "Хэш-префикс"

#: tools/gdbmshell.c:1004
msgid "Bucket address"
msgstr "Адрес корзины"

#: tools/gdbmshell.c:1072
#, c-format
msgid ""
"\n"
"File Header: \n"
"\n"
msgstr ""
"\n"
"Файловый заголовок: \n"
"\n"

#: tools/gdbmshell.c:1073
#, c-format
msgid "  type            = %s\n"
msgstr "  тип                     = %s\n"

#: tools/gdbmshell.c:1074
#, c-format
msgid "  directory start = %lu\n"
msgstr " начало каталога          = %lu\n"

#: tools/gdbmshell.c:1076
#, c-format
msgid "  directory size  = %d\n"
msgstr "  размер каталога         = %d\n"

#: tools/gdbmshell.c:1077
#, c-format
msgid "  directory depth = %d\n"
msgstr "  глубина каталога        = %d\n"

#: tools/gdbmshell.c:1078
#, c-format
msgid "  block size      = %d\n"
msgstr "  размер блока            = %d\n"

#: tools/gdbmshell.c:1079
#, c-format
msgid "  bucket elems    = %d\n"
msgstr "  элементов корзины       = %d\n"

#: tools/gdbmshell.c:1080
#, c-format
msgid "  bucket size     = %d\n"
msgstr "  размер корзины          = %d\n"

#: tools/gdbmshell.c:1081
#, c-format
msgid "  header magic    = %x\n"
msgstr "  идентификатор заголовка = %x\n"

#: tools/gdbmshell.c:1082
#, c-format
msgid "  next block      = %lu\n"
msgstr "  следующий блок          = %lu\n"

#: tools/gdbmshell.c:1085
#, c-format
msgid "  avail size      = %d\n"
msgstr "  доступный размер        = %d\n"

#: tools/gdbmshell.c:1086
#, c-format
msgid "  avail count     = %d\n"
msgstr "  доступное количество    = %d\n"

#: tools/gdbmshell.c:1087
#, c-format
msgid "  avail next block= %lu\n"
msgstr "  доступный след. блок    = %lu\n"

#: tools/gdbmshell.c:1092
#, c-format
msgid ""
"\n"
"Extended Header: \n"
"\n"
msgstr ""
"\n"
"Расширенный заголовок: \n"
"\n"

#: tools/gdbmshell.c:1093
#, c-format
msgid "      version = %d\n"
msgstr "       версия             = %d\n"

#: tools/gdbmshell.c:1094
#, c-format
msgid "      numsync = %u\n"
msgstr "       numsync            = %u\n"

#: tools/gdbmshell.c:1208
msgid "bad file mode"
msgstr "некорректный режим доступа к файлу"

#: tools/gdbmshell.c:1218
msgid "insufficient precision"
msgstr "недостаточная точность"

#. TRANSLATORS: Stands for "Not Available".
#: tools/gdbmshell.c:1231
msgid "N/A"
msgstr "Н/Д"

#: tools/gdbmshell.c:1239 tools/gdbmshell.c:1244
msgid "can't open database"
msgstr "не удалось открыть базу данных"

#: tools/gdbmshell.c:1249
msgid "not a regular file"
msgstr "не обычный файл"

#: tools/gdbmshell.c:1254
msgid "ERROR"
msgstr "ОШИБКА"

#: tools/gdbmshell.c:1264
#, c-format
msgid "%s: ERROR: can't stat: %s"
msgstr "%s: ОШИБКА: невозможно выполнить stat: %s"

#: tools/gdbmshell.c:1288
msgid "Invalid arguments in call to gdbm_latest_snapshot"
msgstr "Некорректные аргументы в вызове gdbm_latest_snapshot"

#: tools/gdbmshell.c:1293
msgid ""
"Function is not implemented: GDBM is built without crash-tolerance support"
msgstr ""
"Функция не реализована: GDBM собрана без поддержки устойчивости к сбоям"

#: tools/gdbmshell.c:1301
msgid "Selected the most recent snapshot"
msgstr "Выбран самый новый снимок"

#: tools/gdbmshell.c:1305
msgid "Neither snapshot is readable"
msgstr "Ни один из снимков не читается"

#: tools/gdbmshell.c:1310
msgid "Error selecting snapshot"
msgstr "Ошибка выбора снимка"

#: tools/gdbmshell.c:1315
msgid "Snapshot modes and dates are the same"
msgstr "Режимы и даты снимков одинаковые"

#: tools/gdbmshell.c:1320
msgid "Snapshot sync counters differ by more than 1"
msgstr "Различие счётчиков синхронизации снимков больше 1"

#: tools/gdbmshell.c:1348
#, c-format
msgid "unexpected error code: %d"
msgstr "неожиданный код ошибки: %d"

#: tools/gdbmshell.c:1368
#, c-format
msgid "hash value = %x, bucket #%u, slot %u"
msgstr "хэш-значение = %x, корзина #%u, слот %u"

#: tools/gdbmshell.c:1374
#, c-format
msgid "hash value = %x"
msgstr "хэш-значение = %x"

#: tools/gdbmshell.c:1427
#, c-format
msgid "unrecognized parameter: %s\n"
msgstr "нераспознанный параметр: %s\n"

#: tools/gdbmshell.c:1434
msgid "select bucket first\n"
msgstr "сначала выберите корзину\n"

#: tools/gdbmshell.c:1485
#, c-format
msgid "error reading entry %d"
msgstr "ошибка чтения элемента %d"

#: tools/gdbmshell.c:1494 tools/gdbmshell.c:1532
msgid "the key was:"
msgstr "ключ был:"

#: tools/gdbmshell.c:1608
msgid "error dumping database"
msgstr "ошибка при создании дампа базы данных"

#: tools/gdbmshell.c:1667
msgid "error restoring metadata"
msgstr "ошибка восстановления метаданных"

#: tools/gdbmshell.c:1700
#, c-format
msgid "Database file: %s\n"
msgstr "Файл базы данных: %s\n"

#: tools/gdbmshell.c:1702
msgid "Database is open"
msgstr "База данных открыта"

#: tools/gdbmshell.c:1704
msgid "Database is not open"
msgstr "База данных не открыта"

#: tools/gdbmshell.c:1759
#, c-format
msgid "unknown debug flag: %s"
msgstr "неизвестный флаг отладки: %s"

#: tools/gdbmshell.c:1762
#, c-format
msgid "invalid type of argument %d"
msgstr "неправильный тип аргумента %d"

#: tools/gdbmshell.c:1767
#, c-format
msgid "Debug flags:"
msgstr "Флаги отладки:"

#: tools/gdbmshell.c:1773
msgid "none"
msgstr "нет"

#: tools/gdbmshell.c:1777
msgid "compiled without debug support"
msgstr "скомпилировано без поддержки отладки"

#: tools/gdbmshell.c:1828
#, c-format
msgid "command failed with status %d"
msgstr "команда завершилась с ошибкой %d"

#: tools/gdbmshell.c:1831
#, c-format
msgid "command terminated on signal %d"
msgstr "команда завершена по сигналу %d"

#. TRANSLATORS: %s is the stream name
#: tools/gdbmshell.c:1901
#, c-format
msgid "input history is not available for %s input stream"
msgstr "история входных данных недоступна для входного потока %s"

#: tools/gdbmshell.c:1994
msgid "count (number of entries)"
msgstr "количество (количество элементов)"

#: tools/gdbmshell.c:2004 tools/gdbmshell.c:2032 tools/gdbmshell.c:2085
#: tools/gdbmshell.c:2194
msgid "KEY"
msgstr "КЛЮЧ"

#: tools/gdbmshell.c:2007
msgid "delete a record"
msgstr "удалить запись"

#: tools/gdbmshell.c:2017 tools/gdbmshell.c:2045 tools/gdbmtool.c:93
msgid "FILE"
msgstr "ФАЙЛ"

#: tools/gdbmshell.c:2022
msgid "export"
msgstr "экспорт"

#: tools/gdbmshell.c:2035
msgid "fetch record"
msgstr "выбрать запись"

#: tools/gdbmshell.c:2050
msgid "import"
msgstr "импорт"

#: tools/gdbmshell.c:2062
msgid "list"
msgstr "список"

#: tools/gdbmshell.c:2072
msgid "[KEY]"
msgstr "[КЛЮЧ]"

#: tools/gdbmshell.c:2075
msgid "continue iteration: get next key and datum"
msgstr "продолжение итерации: получаем следующий ключ и данные"

#: tools/gdbmshell.c:2086
msgid "DATA"
msgstr "ДАННЫЕ"

#: tools/gdbmshell.c:2089
msgid "store"
msgstr "сохранить"

#: tools/gdbmshell.c:2098
msgid "begin iteration: get first key and datum"
msgstr "начало итерации: получить первый ключ и данные"

#: tools/gdbmshell.c:2107
msgid "reorganize"
msgstr "перекомпоновать"

#: tools/gdbmshell.c:2126
msgid "recover the database"
msgstr "восстановить базу данных"

#: tools/gdbmshell.c:2135
msgid "print avail list"
msgstr "напечатать доступный список"

#: tools/gdbmshell.c:2145
msgid "[NUMBER]"
msgstr "[НОМЕР]"

#: tools/gdbmshell.c:2148
msgid "print a bucket"
msgstr "показать корзину"

#: tools/gdbmshell.c:2157
msgid "print current bucket"
msgstr "показать текущую корзину"

#: tools/gdbmshell.c:2166
msgid "print sibling bucket"
msgstr "показать корзину того же уровня"

#: tools/gdbmshell.c:2175
msgid "print hash directory"
msgstr "показать хэш-каталог"

#: tools/gdbmshell.c:2184
msgid "print database file header"
msgstr "показать заголовок файла базы данных"

#: tools/gdbmshell.c:2197
msgid "hash value of key"
msgstr "хэш-значение ключа"

#: tools/gdbmshell.c:2205
msgid "print the bucket cache"
msgstr "показать кэш корзины"

#: tools/gdbmshell.c:2214
msgid "print current program status"
msgstr "показать текущее состояние программы"

#: tools/gdbmshell.c:2222
msgid "Synchronize the database with disk copy"
msgstr "Синхронизировать базу данных с копией на диске"

#: tools/gdbmshell.c:2231
msgid "Upgrade the database to extended format"
msgstr "Обновить базу данных до расширенного формата"

#: tools/gdbmshell.c:2240
msgid "Downgrade the database to standard format"
msgstr "Перевести базу данных в стандартный формат"

#: tools/gdbmshell.c:2254
msgid "analyze two database snapshots"
msgstr "анализировать два снимка базы данных"

#: tools/gdbmshell.c:2262
msgid "print version of gdbm"
msgstr "показать версию gdbm"

#: tools/gdbmshell.c:2270
msgid "print this help list"
msgstr "показать эту справку"

#: tools/gdbmshell.c:2279
msgid "quit the program"
msgstr "завершить работу программы"

#: tools/gdbmshell.c:2291
msgid "set or list variables"
msgstr "задать или показать переменные"

#: tools/gdbmshell.c:2302
msgid "unset variables"
msgstr "удалить переменные"

#: tools/gdbmshell.c:2314
msgid "define datum structure"
msgstr "определить структуру datum"

#: tools/gdbmshell.c:2325
msgid "source command script"
msgstr "источник командного сценария"

#: tools/gdbmshell.c:2333
msgid "close the database"
msgstr "закрыть базу данных"

#: tools/gdbmshell.c:2345
msgid "open new database"
msgstr "открыть базу данных"

#: tools/gdbmshell.c:2354
msgid "[FROM]"
msgstr "[ИЗ]"

#: tools/gdbmshell.c:2355
msgid "[COUNT]"
msgstr "[КОЛИЧЕСТВО]"

#: tools/gdbmshell.c:2358
msgid "show input history"
msgstr "показать историю ввода"

#: tools/gdbmshell.c:2367
msgid "query/set debug level"
msgstr "показать/задать уровень отладки"

#: tools/gdbmshell.c:2386
msgid "invoke the shell"
msgstr "запустить оболочку"

#: tools/gdbmshell.c:2398
msgid "describe GDBM error code"
msgstr "пояснить код ошибки GDBM"

#: tools/gdbmshell.c:2547
msgid "Invalid command. Try ? for help."
msgstr "Недопустимая команда. Введите ? для показа справки."

#: tools/gdbmshell.c:2548
msgid "Unknown command"
msgstr "Неизвестная команда"

#: tools/gdbmshell.c:2863
#, c-format
msgid "cannot coerce %s to %s"
msgstr "невозможно преобразовать %s к %s"

#: tools/gdbmshell.c:3044
#, c-format
msgid "%s: not enough arguments"
msgstr "%s: недостаточно аргументов"

#: tools/gdbmshell.c:3052
msgid "unexpected eof"
msgstr "неожиданный конец файла"

#: tools/gdbmshell.c:3068
#, c-format
msgid "%s: too many arguments"
msgstr "%s: слишком много аргументов"

#: tools/gdbmshell.c:3129
#, c-format
msgid "cannot run pager `%s': %s"
msgstr "не удалось запустить программу просмотра «%s»: %s"

#: tools/gdbmshell.c:3234
#, c-format
msgid ""
"\n"
"Welcome to the gdbm tool.  Type ? for help.\n"
"\n"
msgstr ""
"\n"
"Вас приветствует gdbm. Введите ? для просмотра справки.\n"
"\n"

#: tools/gdbmtool.c:48
msgid "cannot find home directory"
msgstr "не удалось найти домашний каталог"

#: tools/gdbmtool.c:82
msgid "examine and/or modify a GDBM database"
msgstr "просмотр и/или изменение базы данных GDBM"

#: tools/gdbmtool.c:83
msgid "DBFILE [COMMAND [ARG ...]]"
msgstr "ФАЙЛ-БД [КОМАНДА [АРГУМЕНТЫ …]]"

#: tools/gdbmtool.c:91 tools/gdbmtool.c:92
msgid "SIZE"
msgstr "РАЗМЕР"

#: tools/gdbmtool.c:91
msgid "set block size"
msgstr "задать размер блока"

#: tools/gdbmtool.c:92
msgid "set cache size"
msgstr "задать размер кэша"

#: tools/gdbmtool.c:93
msgid "read commands from FILE"
msgstr "читать команды из ФАЙЛА"

#: tools/gdbmtool.c:95
msgid "disable file locking"
msgstr "выключить блокировку файла"

#: tools/gdbmtool.c:96
msgid "do not use mmap"
msgstr "не использовать mmap"

#: tools/gdbmtool.c:97
msgid "create database"
msgstr "Создать базу данных"

#: tools/gdbmtool.c:98
msgid "do not read .gdbmtoolrc file"
msgstr "не читать файл .gdbmtoolrc"

#: tools/gdbmtool.c:99
msgid "open database in read-only mode"
msgstr "открыть базу данных только на чтение"

#: tools/gdbmtool.c:100
msgid "synchronize to disk after each write"
msgstr "синхронизировать на диске после каждой записи"

#: tools/gdbmtool.c:101
msgid "don't print initial banner"
msgstr "не показывать начальную заставку"

#. TRANSLATORS: File Descriptor.
#: tools/gdbmtool.c:104
msgid "FD"
msgstr "FD"

#: tools/gdbmtool.c:105
msgid "open database at the given file descriptor"
msgstr "открыть базу данных из указанного файлового дескриптора"

#: tools/gdbmtool.c:106
msgid "extended format (numsync)"
msgstr "расширенный формат (numsync)"

#: tools/gdbmtool.c:108
msgid "enable trace mode"
msgstr "включить режим трассировки"

#: tools/gdbmtool.c:109
msgid "print timing after each command"
msgstr "печатать время после каждой команды"

#: tools/gdbmtool.c:111
msgid "enable lexical analyzer traces"
msgstr "включить трассировку лексического анализатора"

#: tools/gdbmtool.c:112
msgid "enable grammar traces"
msgstr "включить трассировку грамматики"

#: tools/gdbmtool.c:149
#, c-format
msgid "invalid file descriptor: %s"
msgstr "неверный файловый дескриптор: %s"

#: tools/gdbmtool.c:226
#, c-format
msgid "unknown option %s; try `%s -h' for more info"
msgstr "неизвестный параметр %s; для показа справки введите «%s -h»"

#: tools/gdbmtool.c:229
#, c-format
msgid "unknown option %c; try `%s -h' for more info"
msgstr "неизвестный параметр %c; для показа справки введите «%s -h»"

#: tools/gdbmtool.c:253
msgid "--file and command cannot be used together"
msgstr "--file и команду нельзя использовать вместе"

#: tools/gram.y:179
#, c-format
msgid "duplicate tag: %s"
msgstr "повторный тег: %s"

#: tools/gram.y:261
#, c-format
msgid "expected \"key\" or \"content\", but found \"%s\""
msgstr "ожидалось «key» или «content», но найдено «%s»"

#: tools/gram.y:333 tools/gram.y:362 tools/gram.y:394
#, c-format
msgid "no such variable: %s"
msgstr "нет переменной с именем: %s"

#: tools/gram.y:337
#, c-format
msgid "%s is not a boolean variable"
msgstr "%s не является булевой переменной"

#: tools/gram.y:341
#, c-format
msgid "%s: setting is not allowed"
msgstr "%s: значение недопустимо"

#: tools/gram.y:345
msgid "can't set variable"
msgstr "невозможно задать переменную"

#: tools/gram.y:349 tools/gram.y:374
#, c-format
msgid "unexpected error setting %s: %d"
msgstr "неожиданный ошибка при назначении %s: %d"

#: tools/gram.y:366
#, c-format
msgid "%s: bad variable type"
msgstr "%s: неправильный тип переменной"

#: tools/gram.y:370
#, c-format
msgid "%s: value %s is not allowed"
msgstr "%s: значение %s не допускается"

#: tools/gram.y:398
#, c-format
msgid "%s: variable cannot be unset"
msgstr "%s: переменную нельзя удалить"

#: tools/input-file.c:60
#, c-format
msgid "cannot open `%s': %s"
msgstr "не удалось открыть «%s»: %s"

#: tools/input-file.c:65
#, c-format
msgid "%s is not a regular file"
msgstr "%s не является обычным файлом"

#: tools/input-file.c:72
#, c-format
msgid "cannot open %s for reading: %s"
msgstr "невозможно открыть %s для чтения: %s"

#: tools/lex.l:146
msgid "recursive sourcing"
msgstr "рекурсивный источник"

#: tools/lex.l:148
#, c-format
msgid "%s already sourced here"
msgstr "источник  уже указан здесь %s"

#: tools/lex.l:259 tools/lex.l:269
msgid "invalid #line statement"
msgstr "неправильное определение #line"

#: tools/parseopt.c:49
msgid "give this help list"
msgstr "вывод этой справки"

#: tools/parseopt.c:50
msgid "print program version"
msgstr "вывод версии программы"

#: tools/parseopt.c:51
msgid "give a short usage message"
msgstr "вывод короткого сообщения об использовании"

#: tools/parseopt.c:289
#, c-format
msgid "error in ARGP_HELP_FMT: improper usage of [no-]%s\n"
msgstr "ошибка в ARGP_HELP_FMT: некорректное использование [no-]%s\n"

#: tools/parseopt.c:307
#, c-format
msgid "error in ARGP_HELP_FMT: bad value for %s"
msgstr "ошибка в ARGP_HELP_FMT: некорректное значение %s"

#: tools/parseopt.c:311
#, c-format
msgid " (near %s)"
msgstr " (около %s)"

#: tools/parseopt.c:320
#, c-format
msgid "error in ARGP_HELP_FMT: %s value is out of range\n"
msgstr "ошибка в ARGP_HELP_FMT: значение %s вне диапазона\n"

#: tools/parseopt.c:331
#, c-format
msgid "%s: ARGP_HELP_FMT parameter requires a value\n"
msgstr "%s: для параметра ARGP_HELP_FMT требуется значение\n"

#: tools/parseopt.c:340
#, c-format
msgid "%s: Unknown ARGP_HELP_FMT parameter\n"
msgstr "%s: неизвестный параметр ARGP_HELP_FMT\n"

#: tools/parseopt.c:367
#, c-format
msgid "ARGP_HELP_FMT: missing delimiter near %s\n"
msgstr "ARGP_HELP_FMT: отсутствует разделитель рядом с %s\n"

#: tools/parseopt.c:487 tools/parseopt.c:565
msgid "Usage:"
msgstr "Использование:"

#: tools/parseopt.c:489
msgid "OPTION"
msgstr "ПАРАМЕТР"

#: tools/parseopt.c:510
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Обязательные или необязательные аргументы длинных параметров также являются "
"обязательными или необязательными соответствующих коротких параметров."

#. TRANSLATORS: The placeholder indicates the bug-reporting address
#. for this package.  Please add _another line_ saying
#. "Report translation bugs to <...>\n" with the address for translation
#. bugs (typically your translation team's web or email address).
#: tools/parseopt.c:523
#, c-format
msgid "Report bugs to %s.\n"
msgstr "Сообщения об ошибках отправляйте по адресу %s.\n"

#: tools/parseopt.c:526
#, c-format
msgid "%s home page: <%s>\n"
msgstr "Домашняя страница %s: <%s>\n"

#. TRANSLATORS: Translate "(C)" to the copyright symbol
#. (C-in-a-circle), if this symbol is available in the user's
#. locale.  Otherwise, do not translate "(C)"; leave it as-is.
#: tools/parseopt.c:667
msgid "(C)"
msgstr "©"

#. TRANSLATORS: Please, don't translate 'y' and 'n'.
#: tools/util.c:100
msgid "Please, reply 'y' or 'n'"
msgstr "Введите «y» или «n»"

#: tools/var.c:792
#, c-format
msgid "unrecognized error code: %s"
msgstr "неожиданный код ошибки: %s"

#~ msgid ""
#~ "bits = %d\n"
#~ "count= %d\n"
#~ "Hash Table:\n"
#~ msgstr ""
#~ "бит = %d\n"
#~ "количество= %d\n"
#~ "Хэш-таблица:\n"

#~ msgid "Current bucket"
#~ msgstr "Текущая корзина"

#~ msgid " current bucket address  = %lu.\n"
#~ msgstr " адрес текущей корзины = %lu.\n"

#~ msgid "  table        = %lu\n"
#~ msgstr "  таблица                 = %lu\n"

#~ msgid "  table size   = %d\n"
#~ msgstr "  размер таблицы          = %d\n"

#~ msgid "  table bits   = %d\n"
#~ msgstr "  битов таблицы           = %d\n"

#~ msgid "Illegal data"
#~ msgstr "Неверные данные"

#~ msgid "Illegal option"
#~ msgstr "Недопустимый параметр"

#~ msgid "warning: using default database file %s"
#~ msgstr "предупреждение: используется файл базы данных по умолчанию %s"

#~ msgid "Not a bucket."
#~ msgstr "Не корзина."

#~ msgid "cannot load from %s: %s"
#~ msgstr "невозможно загрузить из %s: %s"

#~ msgid "No database name"
#~ msgstr "Нет имени базы данных"

#~ msgid ""
#~ "\n"
#~ "header block\n"
#~ "size  = %d\n"
#~ "count = %d\n"
#~ msgstr ""
#~ "\n"
#~ "блок заголовка\n"
#~ "размер  = %d\n"
#~ "количество = %d\n"

#~ msgid "couldn't init cache"
#~ msgstr "не удалось инициализировать кэш"

#~ msgid "invalid avail_block"
#~ msgstr "некорректный avail_block"

#~ msgid "nextkey"
#~ msgstr "следующий ключ"

#~ msgid "firstkey"
#~ msgstr "первый ключ"
